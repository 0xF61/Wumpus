import javax.xml.stream.Location;
import java.util.Scanner;

public class Player {

	// the characters for possible actions
	public static final char UP = 'u';
	public static final char DOWN = 'd';
	public static final char RIGHT = 'r';
	public static final char LEFT = 'l';
	public static final char GRAB = 'g';
	public static final char SHOOT = 's';
	public static final char CLIMB = 'c';

	private int points;		// that is the performance measure of the player, increases by gold and decreases by various costs that are defined as constants in WumpusWorld.java
	private Cell location;	// the location of the player (the current cell where he is in)
	private Map map;		// stores the perceived environment by the player
	private boolean shootArrow;	// keeps whether the player's only arrow is shot or not

	// initialize the field variables of player object
	public Player(Cell c, int mapSize) {
		location = new Cell(c.row, c.col);
		shootArrow = false;		// initially, the player's only arrow is not shot
		map = new Map(mapSize);	// an empty map initially
		map.addContent(location, Map.PLAYER);	// marks its own position
	}

	public Cell getLocation() {
		return new Cell(location.row,location.col);
	}

	// See What has your around
	public void perceiveCell(String contents) {
		map.addContent(new Cell(location.row,location.col),contents);
	}

	public void viewMap() {
		map.printMap();
	}

	// Check the action can do 
	public boolean isActionPossible(char action) {
		if (action == Player.CLIMB) {
			Cell place = getLocation();
			if (place.col == 1 && place.row == 1)
				return true;
		}
		else if (action == Player.UP) {
			if (location.row > 1)
				return true;
			return false;
		}
		else if (action == Player.DOWN) {
			if (location.row < map.getSize()) // 4
				return true;
			return false;
		}
		else if (action == Player.LEFT) {
			if (location.col > 1)
				return true;
			return false;
		}
		else if (action == Player.RIGHT) {
			if (location.col < map.getSize())
				return true;
			return false;
		}
		else if (action == Player.GRAB) {
			if ( map.contains(location, Map.GOLD) )
				return true;
			return false;

		}
		else if (action == Player.SHOOT) {
			return !shootArrow;
		}
		return false;
	}

	// If action is can be perform that action
	public void perform(char action) {

		if (action == Player.UP) {
			map.removeContent(new Cell(location.row,location.col), map.PLAYER);
			location.row--;
		}
		else if (action == Player.DOWN) {
			map.removeContent(new Cell(location.row,location.col), map.PLAYER);
			location.row++;
		}
		else if (action == Player.LEFT) {
			map.removeContent(new Cell(location.row,location.col), map.PLAYER);
			location.col--;
		}
		else if (action == Player.RIGHT) {
			map.removeContent(new Cell(location.row,location.col), map.PLAYER);
			location.col++;
        }
		else if (action == Player.GRAB) {
			map.removeContent(new Cell(location.row,location.col), map.GOLD);
		}
		else if (action == Player.SHOOT) {
			shootArrow = true;
		}
	}

	// Every movement has Cost
	public void dropCost(int cost) {
		points = points - cost;
		//0      0      -  -1000
	}

	// Return the Player's point 
	public String getPoints() {
		return points+"";
	}

}
