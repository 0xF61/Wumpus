import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WumpusWorld {

	public static final String WUMPUS_DATA_FILE = "wumpus-data.txt";

	// costs and utilities in the game
	public static final int FALLING_INTO_PIT_COST = 1000;
	public static final int EATEN_BY_WUMPUS_COST = 1000;
	public static final int SHOOTING_ARROW_COST = 10;
	public static final int STEP_COST = 1;
	public static final int GOLD_UTILITY = 1000;


	public static void main(String[] args) throws FileNotFoundException {

		// create a map for the program
		Map wumpusMap = new Map(new Scanner(new File(WUMPUS_DATA_FILE)));

		// create a Player object to represent the player in the game
		Player player = new Player(new Cell(1, 1), wumpusMap.getSize());
		// the player perceives its environment that is the cell in which he is currently
		player.perceiveCell(wumpusMap.getContents(player.getLocation()));

		// create a scanner object to receive input from the user that is the actual player
		Scanner input = new Scanner(System.in);

		// game continues until
		// (1) the player is eaten by the wumpus
		// (2) OR the player falls into a pit
		// (3) OR the player climbs out of the cave
		while(true) {

			// display the player's map (perceived map)
			player.viewMap();
//            wumpusMap.contains(player.getLocation(), )
            // get a valid input (a possible one) for the action from the user
			char action;
			do {
				System.out.print("Please enter your move choice (u)p, (d)own, (r)ight, (l)eft, (g)rab, (s)hoot, (c)limb: ");
				action = input.next().toLowerCase().charAt(0);
			} while(!player.isActionPossible(action));

			// player performs the valid action
			player.perform(action);

			// finalize the game if the player is eaten by the wumpus
			if(wumpusMap.contains(player.getLocation(), Map.PIT)) {
    			player.dropCost(FALLING_INTO_PIT_COST);
				System.out.println("You fell into a pit, the game is over with " + player.getPoints() +" points!");
				break;

			// finalize the game if the player falls into a pit
			} else if(wumpusMap.contains(player.getLocation(), Map.WUMPUS)) {
				player.dropCost(EATEN_BY_WUMPUS_COST);
				System.out.println("The wumpus ate you, the game is over with " + player.getPoints() +" points!");
				break;

			// finalize the game if the player climbs out of the cave
			} else if(action == Player.CLIMB) {
				System.out.println("You climbed out of the cave, the game is over with " + player.getPoints() +" points!");
				break;

			// remove the gold from the real map after the player grabs it
			} else if(action == Player.GRAB) {
				System.out.println("You grabbed the gold successfully!");
				wumpusMap.removeContent(player.getLocation(), Map.GOLD);
                player.dropCost( 0-GOLD_UTILITY );

			// replace the wumpus with its corpse if the player shoots the wumpus from an adjacent cell
			} else if(action == Player.SHOOT) {
                    System.out.println("Where do you want to Shoot?");
                    System.out.print("(u)p, (d)own, (r)ight, (l)eft: ");
                    char choice = input.next().toLowerCase().charAt(0);
                    switch (choice) {
                    case Player.UP:
                        player.dropCost(SHOOTING_ARROW_COST);
                        wumpusMap.removeAdjacentContent(player.getLocation(), Map.WUMPUS, choice);
                        break;
                    case Player.DOWN:
                        player.dropCost(SHOOTING_ARROW_COST);
                        wumpusMap.removeAdjacentContent(player.getLocation(), Map.WUMPUS, choice);
                        break;
                    case Player.RIGHT:
                        player.dropCost(SHOOTING_ARROW_COST);
                        wumpusMap.removeAdjacentContent(player.getLocation(), Map.WUMPUS, choice);
                        break;
                    case Player.LEFT:
                        player.dropCost(SHOOTING_ARROW_COST);
                        wumpusMap.removeAdjacentContent(player.getLocation(), Map.WUMPUS, choice);
                        break;
                    default:
                        break;
                    }

			// the player perceives the cell he just moved into and updates its own map (perceived map)
			} else {
				System.out.print("You moved ");
                player.dropCost(STEP_COST);
				switch (action) {
				case Player.UP:
					System.out.print("up");
					break;
				case Player.DOWN:
					System.out.print("down");
					break;
				case Player.RIGHT:
					System.out.print("right");
					break;
				case Player.LEFT:
					System.out.print("left");
					break;
				default:
					break;
				}
				System.out.println(" successfully!");
				player.perceiveCell(wumpusMap.getContents(player.getLocation()));
            }
		}	// end of while
	} // end of main
}	// end of class
