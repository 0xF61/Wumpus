import java.util.Arrays;
import java.util.Scanner;

public class Map {

	// colors defined for colorful output on console
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";

	// possible objects and perceptions on the map of wumpus world
	public static final String PIT = "pit";
	public static final String WUMPUS = "wumpus";
	public static final String BREEZE = "breeze";
	public static final String STENCH = "stench";
	public static final String GOLD = "gold";
	public static final String PLAYER = "PLAYER";
	private static final String DEAD_WUMPUS = "corpse";

	// the contents of the map, each element is a String
	// possible contents are "wumpus", "stench breeze gold", "breeze PLAYER", and so on
	private String map[][];
	// the edge size of the grid
	private int size;


	public Map(int mapSize) {
		// initialize the edge size of the grid
		this.size = mapSize;

		// create the two dimensional array
		// for simplicity, we are not using the row zero and column zero,
		// therefore, we need an array with length=size+1
		map = new String[size + 1][size + 1];
		for (int i = 0; i < map.length; i++)
			map[i] = new String[size + 1];

		// initialize the elements of array with empty string
		for (int i = 0; i < map.length; i++)
			Arrays.fill(map[i], "");
	}

	/**
	 * this constructor is used to load the wumpus world
	 * from the given scanner object which reads from a file
	 * */
	public Map(Scanner input) {

		// call the default constructor to create an empty array
		// with the edge size specified in the file
		this(Integer.parseInt(input.nextLine()));

		// read the objects on the wumpus world from file
		while (input.hasNextLine()) {

			String line = input.nextLine();

			Scanner lineParser = new Scanner(line);
			String object = lineParser.next();		// read the object
			int row = lineParser.nextInt();
			int col = lineParser.nextInt();
			map[row][col] = object;
		}

		// add the influences of the objects that are added into the map above
		// for instance add breeze into the adjacent cells of a pit
		// for instance add stench into the adjacent cells of the wumpus
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size; j++) {

				if(map[i][j].equals(PIT) || map[i][j].equals(WUMPUS)) {

					// figure out the right perception based on the trap
					String perception = (map[i][j].equals(PIT) ? BREEZE : map[i][j].equals(WUMPUS) ? STENCH : "");

					/* the perception will be added to each adjacent cell (above, below, right, left)
					   if and only if the adjacent cell does not contain a trap (wumpus or pit)
					   and does not contain the same perception already */

					// add perception into the cell above
					if (i - 1 >= 1 && !isTrap(map[i - 1][j]) && !map[i - 1][j].contains(perception))
						map[i - 1][j] += " " + perception;

					// add perception into the cell below
					if (i + 1 <= size && !isTrap(map[i + 1][j]) && !map[i + 1][j].contains(perception))
						map[i + 1][j] += " " + perception;

					// add perception into the cell on left
					if (j - 1 >= 1 && !isTrap(map[i][j - 1]) && !map[i][j - 1].contains(perception))
						map[i][j - 1] += " " + perception;

					// add perception into the cell on right
					if (j + 1 <= size && !isTrap(map[i][j + 1]) && !map[i][j + 1].contains(perception))
						map[i][j + 1] += " " + perception;
				}
			}
		}
	}

	/**
	 *  this method is used to determine
	 *  whether the content of a cell contains
	 *  a trap (wumpus or pit) or not
	 *
	 * */
	private boolean isTrap(String item) {
		return item.equals(PIT) || item.equals(WUMPUS);
	}

	private String pickColor(String item) {

		if (item.equals(GOLD))
			return ANSI_YELLOW;

		else if (item.equals(WUMPUS) || item.equals(STENCH))
			return ANSI_GREEN;

		else if (item.equals(PIT) || item.equals(BREEZE))
			return ANSI_BLUE;

		else if (item.equals(PLAYER))
			return ANSI_RED;

		else if (item.equals(DEAD_WUMPUS))
			return ANSI_PURPLE;

		else
			return ANSI_BLACK;
	}

	public int getSize() {
		return size;
	}

	public void printMap() {

		System.out.print("+");
		for (int i = 1; i <= size; i++)
			System.out.print("---------+");

		System.out.println();

		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= 4; j++) {
				System.out.print("|");

				for (int k = 1; k <= size; k++) {

					Scanner input = new Scanner(map[i][k]);
					String[] contents = new String[10];
					int n = 0;
					while (input.hasNext()) {
						n++;
						contents[n] = input.next();
					}
					if (n >= j) {
						// choose one of the printout options below by commenting out
						// colored printout
						System.out.printf(pickColor(contents[j]) + "%9s" + ANSI_RESET + "|", contents[j]);
						// black printout
						//System.out.printf("%9s|", contents[j]);
					} else
						System.out.printf("%9s|", "");

				}
				System.out.println();
			}

			System.out.print("+");
			for (int j = 1; j <= size; j++)
				System.out.print("---------+");

			System.out.println();

		}
	}

	// Add Content where the location. 
	// Save the old data because of removin duplicates
	public void addContent(Cell location, String content) {
		Scanner old = new Scanner(map[location.row][location.col]);		
		String oldS = " ";
		while(old.hasNext()){
			oldS = old.next();
			if(!oldS.equals(content))
				oldS += old; 

		}

		map[location.row][location.col] = PLAYER+" "+content;		
	}

	// Content of any cell
	public String getContents(Cell location) {
		return map[location.row][location.col];
	}

	// Is the location has obj inside 
	public boolean contains(Cell location, String obj) {
		Scanner contents = new Scanner(map[location.row][location.col]);
		while(contents.hasNext()){
			String tmp = contents.next();
			if(tmp.equals(obj))
				return true;
		}
		return false;
	}

	// Remove the object on that Cell
	public void removeContent(Cell location, String deleteMe) {
		Scanner old = new Scanner(map[location.row][location.col]);
		map[location.row][location.col] = " ";
		while(old.hasNext()){
			String tmp = old.next();
			if(!tmp.equals(deleteMe)){
				map[location.row][location.col] += " "+tmp;
			}
		}

	}

	// This is for killin wumpus Adjacent Cell
	public void removeAdjacentContent(Cell location, String wumpus, char where) {
		boolean isLiveWumpus = true;
        switch (where) {
            case Player.UP:
			if(map[location.row-1][location.col].equals(WUMPUS) ){
				isLiveWumpus = false;
				map[location.row-1][location.col] = "corpse";
			}
			break;
        case Player.DOWN:
			if(map[location.row+1][location.col].equals(WUMPUS) ){
				isLiveWumpus = false;
				map[location.row+1][location.col] = "corpse";
			}
			break;
        case Player.RIGHT:
			if(map[location.row][location.col+1].equals(WUMPUS) ){
				isLiveWumpus = false;
				map[location.row][location.col+1] = "corpse";
			}break;
	    case Player.LEFT:
			if(map[location.row][location.col-1].equals(WUMPUS) ){
				isLiveWumpus = false;
				map[location.row][location.col-1] = "corpse";
			}break;
        }

        if(!isLiveWumpus){
        	System.out.println("Ahhhhh!");}
        else
        	System.out.println("You missed that shot.");
        }

	}

    

